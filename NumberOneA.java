/**
 * @author  : Fajar Subhan
 * @desc    : Making the number one star program part a
 *
 */

public class NumberOneA
{
    public static void main(String[] data)
    {
        for(int i = 0;i < 3;i++)
        {
            for(int j = 0; j < 5 ;j++)
            {
                /**
                 * Jika i == 1 dan j == 2 maka buat string kosong lalu tambahkan spasi
                 * i dan j itu dari mana nilainya ?
                 * i dan j itu berasal dari increment looping for masing masing
                 *
                 * untuk i itu berasal dari luar atau simplenya dia yang dihitung dari sebelah kiri ke bawah
                 * untuk j itu berasal dari dalam atau simplenya dia dihitung dari sebelah kiri ke kanan
                 *
                 */
                if(i == 1 && j == 2)
                {
                    System.out.print(" ");
                }
                /**
                 * jika i dan j bukan yang dimaksud dari kondisi diatas maka
                 * tampilkan kembali lambang bintangnya
                 */
                else
                {
                    System.out.print("*");
                }

            }

            System.out.print("\n");
        }
    }
}
