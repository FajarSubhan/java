/**
 * @author  : Fajar Subhan
 * @desc    : the number three program part b
 *
 */


import java.util.Scanner;

public class PartTwoB 
{

    public static void main(String[] data)
    {
        Scanner userInput = new Scanner(System.in);
        int angka,hasil;

        System.out.print("\n Masukkan Banyaknya Perulangan yang diinginkan : ");
        angka = userInput.nextInt();

        System.out.print("\n Angka yang habis di bagi 3 dan habis di bagi 5 serta tidak habis dibagi 2 adalah  \n");
        for (int i = 1; i <= angka; i++)
        {
            if (i % 3 == 0 && i % 5 == 0 && i % 2 != 0)
            {
                System.out.print(i + " ");
            }
        }
    }
}
