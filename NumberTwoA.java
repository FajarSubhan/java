/**
 * @authr Fajar Subhan
 * @desc  Making the number two program part a
 *
 * */
public class NumberTwoA
{
    public static void main(String[] data)
    {
        int angka;

        angka = 1;

        for(int i = 0;i < 6;i++)
        {
            for(int j = 0; j < i; j++)
            {
                System.out.print(angka + " ");

                angka++;
            }
            System.out.print("\n");
        }

    }
}
