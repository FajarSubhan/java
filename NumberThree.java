/**
 * @author  : Fajar Subhan
 * @desc    : the number three
 *
 */

import java.util.Scanner;

public class NumberThree
{
    public static void main(String[] data)
    {
        int jml_brng,br_ke,total;

        int     harga[] = new int[15];
        String  nama[]  = new String[15];

        Scanner input = new Scanner(System.in);

        System.out.print(" APLIKASI TOKO");
        System.out.println(">>>>>>>>o<<<<<<<<<<<<");
        System.out.print("Masukan jumlah barang yang ingin dibeli : ");

        jml_brng  = input.nextInt();

        /**
         * Disini kita berikan nilai default untuk variable nama dan harga
         * karena nanti untuk output looping meminta untuk di initialize terlebih dahulu
         *
         * */
        total   = 0;

        /**
         * @Inputan
         *
         * Disini kita buat looping(perulangan) sekaligus inputan
         *
         * */
        for(int i = 0;i < jml_brng;i++)
        {
            System.out.print("Barang Ke : ");
            br_ke       = input.nextInt();

            System.out.print("Nama : ");
            nama[i]     = input.next();

            System.out.print("Harga : ");
            harga[i]    = input.nextInt();
        }

        System.out.println("-----------------------------------------------------");

        System.out.println("DAFTAR BELANJA");
        System.out.println("========================");
        System.out.print("No\t\t");
        System.out.print("Nama\t\t");
        System.out.print("Harga\t");
        System.out.print("\n");
        for(int j = 0;j < jml_brng;j++)
        {

            System.out.print(j + 1 + "\t\t");
            System.out.print(nama[j] + "\t\t");
            System.out.print(harga[j] + "\n");
            total += harga[j];
        }

        System.out.println("------------------------------------------");
        System.out.print("Jadi total harga yang harus dibayar : Rp " + total);
    }
}
