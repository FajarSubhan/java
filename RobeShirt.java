/**
 * @author  : Fajar Subhan
 * @desc    : UAP Pemrograman 2
 * @npm     : 202043500578
 * @Help    : Nama Class Tolong di ganti jangan sama
 * */

/* Import Scanner Class */
import java.util.Scanner;

public class RobeShirt
{
  public static void main(String[] data)
  {
      /* ---------- start variable declaration ------------------- */
      int first;
      int last;
      int total;
      int capital;
      int advantage;

      Scanner inputUser = new Scanner(System.in);
      /* ---------- end variable declaration ------------------- */

      System.out.println("=== PERHITUNGAN Penjualan Baju Gamis ( Selama Ramadhan 1441 H ) - GAMIS FASHION ===");
      System.out.println("|==========================================================================|");

      /* Input First */
      System.out.print("Silahkan input Jumlah Penjualan Baju Gamis Minggu Pertama : ");
      first = inputUser.nextInt();

      System.out.println("|==========================================================================|");

      /* Input Last */
      System.out.print("Silahkan input Jumlah Penjualan Baju Gamis Sampai Minggu Terakhir : ");
      last = inputUser.nextInt();

      System.out.println("|==========================================================================|");
      System.out.println("|                                                                          |");
      System.out.println("|                                                                          |");
      System.out.println("v                                                                          v");

      /* Proses Total */
      total = first + last;
      System.out.printf("                   %s = %d","Total Penjualan adalah",total);

      System.out.println("");
      System.out.println("");

      /* ---------- Output --------- */

      // Ketentuan No 1
      if(total == 100)
      {
          System.out.println("Anda melebihi mencapai target penjualan sebulan");
          System.out.println("=====================================");
          System.out.println("Dengan rincian harga modal Rp.80.000, dijual Rp.150.000");

          capital =  total * 80000;
          System.out.printf("%s = %d %s","Modal",capital," Rupiah");

          System.out.println("");

          advantage = total * 70000;
          System.out.printf("%s = %d %s","Keuntungan",advantage," Rupiah");
      }

      // Ketentuan No 2
      else if(total < 100)
      {
          System.out.println("Anda tidak mencapai target penjualan sebulan");
          System.out.println("=====================================");
          System.out.println("Dengan rincian harga modal Rp.80.000, dijual Rp.150.000");

          capital =  total * 80000;
          System.out.printf("%s = %d %s","Modal",capital," Rupiah");

          System.out.println("");

          advantage = total * 70000;
          System.out.printf("%s = %d %s","Keuntungan",advantage," Rupiah");
      }

      // ketentuan No 3
      else if(total > 100)
      {
          System.out.println("Anda melebihi mencapai target penjualan sebulan");
          System.out.println("=====================================");
          System.out.println("Dengan rincian harga modal Rp.80.000, dijual Rp.150.000");

          capital =  total * 80000;
          System.out.printf("%s = %d %s","Modal",capital," Rupiah");

          System.out.println("");

          advantage = total * 70000;
          System.out.printf("%s = %d %s","Keuntungan",advantage," Rupiah");
      }
  }
}
